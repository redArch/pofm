#include <stdio.h>
#include <unistd.h>
#include "main.h"

// TODO
// handle directories
// user should be able to specify -r
// to remove directories recursively
// otherwise print error when removing
// directory

// use printError and printMessage functions

// delete [fileName]
void deleteFile(char** args, int size)
{
	int result;

    // Error handling case: 0 arguments
	if(size == 0)
	{
		printError("Error: Expected at least 1 argument. Use 'help delete' for usage.\n");
	}
	else
	{
        // loop to delete arguments
		for(int i=0; i<size; i++)
		{
		    result = remove(args[i]);
            // result = 0 if successful
		    if(result == 0)
		    {
		        printMessage("%s deleted successfully.\n", args[i]);
		    }
            // error handling case: file doesn't exist
		    else if (access(args[i], F_OK) == -1)
		    {
		        printError("Error: File '%s' does not exist.\n", args[i]);
		    }
            // error handling: other errors
		    else
		    {
		        printError("Failed to delete '%s'.\n", args[i]);
		    }
		}
	}
}

// move [oldPath/fileName] [newPath/fileName]
void moveFile(char** args, int size)
{
	switch (size)
	{
		case 2:
		{
            // error handling case: file doesn't exist
			if (fileInfo(args[0]) == FILE_DNE)
			{
				printError("\nError: File '%s' does not exist.\n\n", args[0]);
				return;
			}
            // successful case
			if(rename(args[0], args[1]))
			{
			    printMessage("File moved successfully.\n");
			    return;
			}
            // error handling: other errors
			else
			{
			    printError("\nFailed to move file.\n\n");
                return;
			}
		}
        // print error if arguments received != 2
		default:
		{
			printError("\nError: Expected 2 arguments. Use 'help move' for usage.\n\n");
			return;
		}
	}
}

// print current version of pofm
void version(__attribute__((unused)) char** args, __attribute__((unused)) int size)
{
    printf("pofm version %s\n", VERSION);
}