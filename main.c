#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>
#include "main.h"

// handle ctrl+c event to quit
void signalHandler(__attribute__((unused)) int signal) {printf("\n"); quit(NULL, 0);}

// directly modifies command to an array of strings (args)
// and returns the size of the array
int commandToArgs(char* command, char** args);

// add an argument to args
// returns 0 on failure
int addArg(char** args, int* argsSize, char* startOfArg);

// replaces an escape sequence like "\n"
// with the literal '\n'
int handleEscaped(char* command, int position);

// when run with args, only run one command
void handleOneOffCommand(int size, char** args);

// all available commands using the command structure
// {"command", "brief description of command", "complete description of all options", commandFunction}
Command commands[] = {
						 {"help", "List all commands and their description", "help [command]\tsee usage of 'command'", help},
						 {"exit", "Quit pofm", "exit\twill quit pofm", quit},
						 {"new", "Create a new file/directory", "new [name]\tcreates a file with 'name'\nnew -d [name]\tcreates a directory with 'name'", newFile},
						 {"delete", "Delete a file or directory", "delete [name]\tdeletes a file/directory with 'name'", deleteFile},
						 {"rename", "Rename a file or directory", "rename [old name] [new name]\trenames a file/directory 'old name' to 'new name'", renameFile},
						 {"copy", "Copies a file or directory", "copy [name] [destination]\tcopies a file with 'name' to 'destination'", copyFile},
						 {"move", "Moves a file", "move [old name] [new name]\tmoves a file/directory 'old name' to 'new name'", moveFile},
						 {"text", "Modify text files", "text -a [text] [file]\tappend 'text' to 'file'\ntext -i [text] [file] [pos]\tinsert 'text' into 'file' at 'pos'\ntext -r [file]\tremove all text in 'file'\ntext -d [height] [file]\tdisplay the contents of 'file' with page size of 'height' (0 for ideal size)", textFile},
						 {"cd", "Change the current directory", "cd [directory]\tchange the current working directory to 'directory'", cd},
						 {"ls", "List files", "ls\tlists all files in current directory\nls [directory]\tlists all files in 'directory'", ls},
						 {"version", "Display pofm version", "version\tDisplays current version of pofm", version}
					 };

// size of the commands array
int commandsSize = sizeof(commands) / sizeof(Command);

// define stuff for windows color support
#if defined(_WIN32) || defined(_WIN64)
HANDLE con;
#endif

// main funtion which will parse
// args and call appropriate functions
int main(int size, char** passedArgs)
{
	// when run with args, only run one command
	if (size > 1)
	{
		handleOneOffCommand(size, passedArgs);
		return 0;
	}

	// when ctrl+c hit call signalHandler to quit
	signal(SIGINT, signalHandler);

	// print welcome message
	// windows is bad and does not support rgb ansi
	// so a bunch of extra stuff has to be done
	// and it's still not full rgb
	#if defined(_WIN32) || defined(_WIN64)

	// get stdhandle 
	con = GetStdHandle(STD_OUTPUT_HANDLE);

	// set text to green
	SetConsoleTextAttribute(con, FOREGROUND_GREEN);

	printf("\n\tWelcome to pofm\n\n");

	// reset to white
	SetConsoleTextAttribute(con, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);

	// print color for MacOS
	#elif defined(__APPLE__)

	printf("\n\t\x1b[32mWelcome to pofm\x1b[0m\n\n");	

	#else

	// print welcome message for users with proper ansi support
	printf("\n\t\x1b[31;1m\033[38;2;64;196;64mWelcome to pofm\x1b[0m\n\n");	

	#endif

	// current working directory
	char* cwd = malloc(sizeof(char) * PATH_MAX);

	// full string user types in (function + args)
	char* command = malloc(sizeof(char) * COMMAND_MAX);

	// first word of command, such as new, quit, or delete
	char* function = malloc(sizeof(char) * FUNCTION_MAX);

	// pointers to point to args in command (array of strings)
	char** args = malloc(sizeof(char*) * ARGS_MAX);

	// loop until the user quits
	while (1)
	{
		// get the current working directory
		getcwd(cwd, PATH_MAX);

		// print prompt
		#if defined(_WIN32) || defined(_WIN64)
		SetConsoleTextAttribute(con, FOREGROUND_GREEN | FOREGROUND_BLUE);
		
		printf("%s >", cwd);

		SetConsoleTextAttribute(con, FOREGROUND_GREEN);

		printf(" pofm > ");

		SetConsoleTextAttribute(con, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);

		#elif (__APPLE__)
		printf("\x1b[36m%s > \x1b[32mpofm > \x1b[0m", cwd);
		#else
		printf("\033[38;2;64;255;255m%s > \033[38;2;64;196;64mpofm > \033[0m", cwd);
		#endif

		// track if the command was too large
		*(command + COMMAND_MAX - 2) = '\n';

		// get the user input and account for error in
		// reading the string
		if (fgets(command, COMMAND_MAX, stdin) != NULL && *command != '\n' && *(command + COMMAND_MAX - 2) == '\n')
		{
			// set first char of function to '\0'
			// to check if a valid sized function was entered
			*function = '\0';

			// start of the function
			// in case there is leading space
			int start = -1;

			// find the function (first word of command)
			for (int i = 0; i < FUNCTION_MAX; i++)
			{
				// break from the loop when function is found
				int doBreak = 0;

				// when end of first word is found it is the function
				switch (*(command + i))
				{
					case ' ':
					case '\0':
					case '\n':
					{
						// only accept the end of the word if the start has been found
						// this accounts for leading space
						if (start >= 0)
						{
							// copy first word of command to function
							strncpy(function, command + start, i);
	
							// set the end of the string at i - start (length of command)
							*(function + i - start) = '\0';
	
							doBreak = 1;
						} else // fill in the leading space to make parsing easier later
						{
							*(command + i) = '0';
						}

						break;
					}

					// found a valid start char
					default:
					{
						// only set if not previously set
						if (start < 0) start = i;

						break;
					}
				}

				// break from loop when function is found
				if (doBreak) break;
			}

			// if function given was greater than FUNCTION_MAX or if no function
			// given chars then first char will be \0
			if (*function != '\0')
			{
				// track if the function given was 
				// found in the array of commands
				int commandFound = 0;

				// compare the function given with all the
				// possible commands
				for (int i = 0; i < commandsSize; i++)
				{
					// strcmp returns 0 when true, so must invert with !
					if (!strcmp(function, (commands + i)->name))
					{
						// parse command into an array of strings (args)
						// returns < 0 if there is an error
						// otherwise returns size of args
						int size = commandToArgs(command, args);

						// call the matching function with the given args
						// commandToArgs will convert command (string) to an
						// array of strings and return it's size
						if (size >= 0) (commands + i)->function(args, size);

						// command was found
						commandFound = 1;

						// now that command was matched break out of loop
						break;
					}
				}

				// command was not found print error
				if (!commandFound)
				{
					printError("\nCommand '%s' was not found, use 'help' to list available commands.\n\n", function);
				}
			}
			// if function given was too large print error
			else
			{
				printError("\nFunction given was invalid (too large), please try again or use 'help' to list available commands.\n\n");
			}
		}
		// command was too large
		else if (*(command + COMMAND_MAX - 2) != '\n')
		{
			printError("\nError: Command was too large.\n\n");
			// clear stdin
			char c;
			while ((c = getchar()) != '\n' && c != EOF);
		}
		// error if fgets null and command was not new line
		else if (*command != '\n')
		{
			printError("\nError: Reading your command.\n\n");
		}
	}
}

int commandToArgs(char* command, char** args)
{
	// keep track of where in the string we are
	int stringTracker = 0;

	// keep track of how many args have been added
	int argsSize = 0;

	// track if the end was found
	int foundEnd = 0;

	// find the end of the function (start of the args)
	while(!foundEnd)
	{
		// move stringTracker to the end of the function
		// if no args after command return argsSize (0)
		switch (*(command + stringTracker))
		{
			case ' ':
			{
				// if the next char is valid (not space, null, or newline)
				// then start of args is found
				if (*(command + stringTracker + 1) != ' ' && *(command + stringTracker + 1) != '\0' && *(command + stringTracker + 1) != '\n') foundEnd = 1;
				break;
			}
			case '\0':
			{
				// end of command was reached no args found
				// return argsSize (0)
				return argsSize;
			}
		}

		// increment position in sreing
		stringTracker++;
	}

	// move string tracker back 1
	// so the first char seen by the next
	// part is ' '
	stringTracker--;

	do
	{
		switch (*(command + stringTracker))
		{
			// handle escaped char
			case '\\':
			{
				// error in escape return
				if(!handleEscaped(command, stringTracker)) return -1;
				break;
			}
			// handle quoted strings
			case '\'':
			{
				// increment to next char so quote is not included in arg
				stringTracker++;

				// the char before the quote should be null
				// as it would have been replaced by the case
				// for spaces
				if (*(command + stringTracker - 2) != '\0')
				{
					// print error and return
					printError("\nQuote in the middle of an argument, at char %d.\n\n", stringTracker);
					return -1;
				}

				// store position of quote to print error if needed
				int quotePosition = stringTracker;

				// check that next char is not invalid
				switch (*(command + stringTracker))
				{
					// two quotes next to each other, arg should not be blank
					case '\'':
					{
						printError("\nEmpty set of quotes at char %d.\n\n", stringTracker);
						return -1;
					}
					// trailing quote
					case '\0':
					{
						printError("\nTrailing quote at char %d.\n\n", stringTracker);
						return -1;
					}
					// is valid, add it to args
					default:
					{
						if (!addArg(args, &argsSize, command + stringTracker)) return -1;
					}
				}

				// go until the end quote is found
				while (*(command + stringTracker) != '\'' && *(command + stringTracker) != '\0')
				{
					// escape sequence found, send it to the handle function
					if (*(command + stringTracker) == '\\' && !handleEscaped(command, stringTracker)) return -1;
					stringTracker++;
				}

				// check the end of the quote
				switch (*(command + stringTracker))
				{
					// if the loop did not end on a quote then quote was not closed
					// so print error
					case '\0':
					{
						printError("\nUnfinished quote at char %d.\n\n", quotePosition + 1);
						return -1;
					}
					// quote is finished so make that the end
					// replacing the quote so it is not included
					// in the args
					case '\'':
					{
						*(command + stringTracker) = '\0';
						break;
					}
				}

				break;
			}
			// replace spaces with null to split string
			// into multiple strings for the args array
			case ' ':
			{
				// set end of an arg
				*(command + stringTracker) = '\0';

				// only add arg when next char is a valid start of an arg
				if (*(command + stringTracker + 1) != ' ' && *(command + stringTracker + 1) != '\0' && *(command + stringTracker + 1) != '\'' && *(command + stringTracker + 1) != '\n')
				{
					if(!addArg(args, &argsSize, command + stringTracker + 1)) return -1;
				}

				// handle escaped char as first character of an argument
				else if (*(command + stringTracker + 1) == '\\')
				{
					// if error return
					if (!handleEscaped(command, stringTracker + 1)) return -1;
					// if start of arg is not null, add it
					else if (*(command + stringTracker) != '\0')
					{
						if(!addArg(args, &argsSize, command + stringTracker + 1)) return -1;
					}
					// if somehow handleEscaped inserted null, handle error
					// this should not happen
					else
					{
						printError("\nError: Something really bad happened.  Check handleEscaped() function.\n\n");
					}
				}

				break;
			}
			// last char of command should be '\n'
			// so make it null to mark end of the args
			case '\n':
			{
				*(command + stringTracker) = '\0';
			}
		}

		// go to next char
		stringTracker++;

	} while (*(command + stringTracker) != '\0'); // loop until end of command string

	// return the number of args found
	return argsSize;
}

// add an arg
int addArg(char** args, int* argsSize, char* startOfArg)
{
	// add arg at startOfArg
	*(args + *argsSize) = startOfArg;

	// increment the number of args
	(*argsSize)++;

	// print an error and return failure when too many args
	if (*argsSize > ARGS_MAX)
	{
		printError("\nToo many args.\n\n");
		return 0;
	}

	// return true when success
	return 1;
}

int handleEscaped(char* command, int position)
{
	// command should not end with a backslash
	if (*(command + position + 1) == '\0')
	{
		printError("\nTrailing \\ at char %d.\n\n", position + 1);
		return 0;
	}

	// overwrite the the backslash
	memmove(command + position, command + position + 1, strlen(command + position));

	// check which char was escaped
	switch(*(command + position))
	{
		// escaped backslash, leave as is
		case '\\': return 1;

		// escaped new line, modify as needed
		case 'n':
		{
			*(command + position) = '\n';
			return 1;
		}
		// escaped tab, modify as needed
		case 't':
		{
			*(command + position) = '\t';
			return 1;
		}
		// escaped quote, leave as is
		case '\'': return 1;

		// escaped space, leave as is
		case ' ': return 1;

		// unrecognized escape sequence (eg \m), print error suggesting to us \\%c to have arg be \%c
		// return error
		default:
		{
			printError("\nUnrecognized escape sequence at char %d, perhaps you meant \\\\%c.\n\n", position + 1, *(command + position));
			return 0;
		}
	}
}

// check the file info (file or directory)
int fileInfo(char* fileName)
{
	struct stat path_stat;
	
	stat(fileName, &path_stat);

	// file does not exist
	if (access(fileName, F_OK) == -1) return FILE_DNE;
	
	// file is a regular file
	else if (S_ISREG(path_stat.st_mode)) return TYPE_REG_FILE;
	
	// file is a directory
	else if (S_ISDIR(path_stat.st_mode)) return TYPE_REG_DIR;
	
	// file is an uknown type
	else return TYPE_OTHER;
}

void handleOneOffCommand(int size, char** args)
{
	// loop through commands, attempting to match
	for (int i = 0; i < commandsSize; i++)
	{
		// match found run the function and return
		if (!strcmp((commands + i)->name, args[1]))
		{
			(commands + i)->function(args + 2, size - 2);
			return;
		}
	}

	// command was not found return
	printError("\nCommand '%s' was not found, use 'help' to list available commands.\n\n", args[1]);
}

// print a message
void printMessage(char* messageFmt, ...)
{
	// accept printf style args
	va_list argslist;
	va_start(argslist, messageFmt);

	printMessageInternal(messageFmt, argslist);

	// free the argslist
	va_end(argslist);
			
}

// print a message
void printMessageInternal(char* messageFmt, va_list argslist)
{
	#if defined(_WIN32) || defined(_WIN64)

	SetConsoleTextAttribute(con, FOREGROUND_RED | FOREGROUND_GREEN);

	// print the message passed using the args
	vprintf(messageFmt, argslist);

	SetConsoleTextAttribute(con, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);

	#elif defined(__APPLE__)

	printf("\x1b[33m");

	vprintf(messageFmt, argslist);

	printf("\x1b[0m");

	#else

	printf("\033[31;1m\033[38;2;196;196;64m");

	// print the message passed using the args
	vprintf(messageFmt, argslist);

	printf("\033[0m");

	#endif
			
}

// print an error
void printError(char* errorFmt, ...)
{
	// accept printf style args
	va_list argslist;
	va_start(argslist, errorFmt); 

	#if defined(_WIN32) || defined(_WIN64)

	SetConsoleTextAttribute(con, FOREGROUND_RED);

	// print the message passed using the args
	vprintf(errorFmt, argslist);

	SetConsoleTextAttribute(con, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);

	#elif defined(__APPPLE__)

	printf("\x1b[31m");
	
	vprintf(messageFmt, argslist);
	
	printf("\x1b[0m");
	
	
	#else

	printf("\033[31;1m\033[38;2;255;64;64m");

	// print the message passed using the args
	vprintf(errorFmt, argslist);

	printf("\033[0m");

	#endif

	va_end(argslist);
}
