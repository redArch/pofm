#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include "main.h"
#include "stddim.h"

// length of the largest command
int largestCommand = -1;

// length of the largest help usage (command [args])
int largestHelpUsageLength = -1;

// asks a user a yes or no question and
// returns 0 for no and 1 for yes
int yesOrNo(char* questionFmt, ...)
{
	va_list argslist;

	// user's response
	char* response = malloc(sizeof(char) * 3);

	*response = '\0';

	// ask what the user wants to do
	while (*response == '\0')
	{
		// get passed args
		va_start(argslist, questionFmt);
		
		// print the question using the
		// internal print function
		printMessageInternal(questionFmt, argslist);
		printMessage(" (Y/n): ");
		
		// get the response
		fgets(response, 3, stdin);

		// change it to lower case
		*response = tolower(*response);

		// check that use has entered 0 or 1 chars
		if (*response != '\n' && *(response + 1) != '\n')
		{
			// print error and reset response
			printError("\nPlease only enter 0 or 1 chars.\n");
			*response = '\0';

			// clear stdin
			char c;
			while ((c = getchar()) != '\n' && c != EOF);
		}

		// only accept when response is valid
		switch (*response)
		{
			// default
			case '\n':
			{
				// ignore fallthrough to next case
				#if defined(__GNUC__) || defined(__GNUG__)
				__attribute__((fallthrough));
				#elif defined(__clang__)
				[[fallthrough]];
				#endif
			}
			// yes
			case 'y':
			{
				free(response);
				va_end(argslist);
				return 1;
			}
			// no
			case 'n': 
			{
				free(response);
				va_end(argslist);
				return 0;
			}
			// error not valid
			default:
			{
				if (*response != '\0')
				{
					printError("\n%c is not a valid choice.\n\n", *response);
					*response = '\0';
				}
				break;
			}
		}

		va_end(argslist);
	}

	// if something goes wrong really wrong return no
	free(response);
	va_end(argslist);
	return 0;
}

// calculate the length of a word
int lengthOfWord(char* c)
{
	int length = 0;

	while (*(c + length) != ' ' && *(c + length) != '\t' && *(c + length) != '\n' && *(c + length) != '\0') length++;

	return length;
}

// list all commands and their description
// or one command and it's options
void help(char** args, int size)
{
	switch (size)
	{
		// if no args print all commands with simple description
		case 0:

			// print help message
			printMessage("\nPofm is a simple file manager that supports several commands listed below.\n\nTo view the usage of a command use 'help [command]' and please note that pofm includes support for escaped sequences like \\n and quoted strings using single quotes (\') when run interactively.\n\n");

			// only calculate largestCommand if it has
			// not been calculated previously
			if (largestCommand < 0)
			{
				// determine the length of the largest command
				for (int i = 0; i < commandsSize; i++)
				{
					int commandSize = strlen((commands + i)->name);
					if (largestCommand < commandSize) largestCommand = commandSize;
				}
			}

			// print the first commands with formatting
			for (int i = 0; i < commandsSize; i++)
			{
				// print the actual command name
				printMessage("\t%s", (commands + i)->name);

				// add some extra space for formatting
				for (int j = 0; j < largestCommand - (int) strlen((commands + i)->name) + 5; j++) printf(" ");

				// print the basic description of the command
				printf("- %s\n", (commands + i)->helpDescription);
			}

			break;

		// 1 arg given assume it's a command and attempt to find it
		case 1:
		{
			// padding between usage and usage description
			int padding = 8;

			// loop through all commands until matching one found
			for (int i = 0; i < commandsSize; i++)
			{
				// if match print the name, description, and options of the command
				if (!strcmp(args[0], (commands + i)->name))
				{
					// only calculate largest usage help length
					// if not previously calculated
					if (largestHelpUsageLength < 0)
					{
						// calculate the length of the help usage (command [args])
						// in order to have uniform offset
						for (int j = 0; j < commandsSize; j++)
						{
							// track usage length
							int currentUsage = 0;
	
							// store the current char					
							char* c = (commands + j)->helpOptions;
	
							// determine which option has the largest length
							while (*c != '\0')
							{
								// \t denotes the description of the usage, so skip
								// and check if the current usage length is greater
								// than the largest, if so make it the largest
								// then reset the current usage for the next line
								if (*c == '\t')
								{
									while (*(c - 1) != '\n' && *c != '\0') c++;
		
									if (currentUsage > largestHelpUsageLength) largestHelpUsageLength = currentUsage;
		
									currentUsage = 0;
		
									// exit loop if end is found
									if (*c == '\0') break;
		
								}
		
								// increment usage and current char
								currentUsage++;
								c++;
							}
						}
					}

					// store the current char
					char* c = (commands + i)->helpOptions;

					// width of the terminal window
					int termWidth = getTermWidth();

					// start the current usage at padding
					// because of the padding printed below
					int currentUsage = padding;
				
					// print the command name and description
					printMessage("%s - %s\n\n", (commands + i)->name, (commands + i)->helpDescription);

					// print initial padding for first usage
					for (int j = 0; j < padding; j++) printf(" ");
					
					// print the usage with description
					while (*c != '\0')
					{
						// wrap the description
						if (currentUsage >= termWidth || lengthOfWord(c) + currentUsage >= termWidth)
						{
							printf("\n");
							currentUsage = 0;
							// print padding so the description lines up
							for (int i = 0; i < largestHelpUsageLength + (padding * 2); i++) 
							{
								printf(" ");
								currentUsage++;
							}

							// skip ' ' and \t after going to newline
							while (*c == ' ' || *c == '\t') c++;
						}
						
						switch (*c)
						{
							// print extra formatting and reset usage
							case '\n':
							{
								printf("\n");
								for (int j = 0; j < padding; j++) printf(" ");
								currentUsage = padding;
								break;
							}
							// print extra space so all text lines up correctly
							case '\t':
							{
								for(int j = currentUsage; j < largestHelpUsageLength + (padding * 2); j++) 
								{
									printf(" ");
									currentUsage++;
								}
								
								break;
							}
							// print the actual string
							default:
							{
								printf("%c", *c);
								currentUsage++;
								break;
							}
						}

						// move to next char
						c++;
					}

					// finish with newline
					printf("\n");

					return;
				}
			}

			// command was not found, print error
			printError("\nError: Command '%s' does not exist, use 'help' to list available commands.\n\n", args[0]);
			break;
		}

		default: // too many arguments, print error
		{
			printError("\nError: Too many arguments (%d), 'help' takes 0 or 1 argument(s).\n\n", size);
			break;
		}
 	}
}

// exit pofm
// __attribute__((unused)) is used to supress unused warning
void quit(__attribute__((unused)) char** args, __attribute__((unused)) int size)
{
	// print exit message
	#if defined(_WIN32) || defined(_WIN64)

	SetConsoleTextAttribute(con, FOREGROUND_GREEN);
	
	printf("\n\tThank you for using pofm.\n\n");

	SetConsoleTextAttribute(con, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);

	#elif defined(__APPLE__)

	printf("\n\t\x1b[32mThank you for using pofm.\x1b[0m\n\n");	
	
	#else

	printf("\n\t\033[31;1m\033[38;2;64;196;64mThank you for using pofm.\n\n");

	#endif
	
	exit(0);
}

void textFile(char** args, int size)
{
	// switch used (-a, -i, -r, -d)
	char operation = '\0';
	int operationPosition;

	for (int i = 0; i < size; i++)
	{
		// find the option(s)
		if (*(args[i]) == '-')
		{
			// if user tries to use multiple options print error
			if (strlen(args[i]) != 2 || operation != '\0')
			{
				printError("\nError: command 'text' takes 1 option.\n\n");
				return;
			}

			// set operation
			operation = *(args[i] + 1);
			operationPosition = i;
		}
	}

	// no operation found
	if (operation == '\0')
	{
		printError("\nError: No operation specified.  Use 'help text' for usage.\n\n");
		return;
	}

	switch (operation)
	{
		// append text to file
		// braces seperate the variable names
		case 'a':
		{
			// should only have 3 args
			if (size != 3)
			{
				printError("\nError: text -a should have 3 arguments including '-a'.  Use 'help text' for usage.\n\n");
				return;
			}

			char* textToAppend;
			char* fileName;

			// -a is the last argument, which makes arguments ambiguous
			// print error and return
			if (operationPosition + 1 >= size)
			{
				printError("\nError: Text to append not specified.  Text should come after '-a'.  Use 'help text' for usage.\n\n");
				return;
			}

			// text will come after -a switch
			textToAppend = args[operationPosition + 1];
			// if -a is the 2nd arg fileName is first, else it is last arg (3rd)
			fileName = operationPosition == 1 ? args[0] : args[2];

			switch (fileInfo(fileName))
			{

				// check file name is a file and not a dir
				case TYPE_REG_DIR:
				{
					printError("\nError: file %s is actually a directory.\n\n", fileName);
					return;
				}

				// unrecognized type of file
				case TYPE_OTHER:
				{
					printError("\nError: file %s is an unrecognized type.\n\n", fileName);
					return;
				}
	
				// file does not exist ask user if they want to create it
				case FILE_DNE:
				{	
					printf("FILENAME: %s\n\n", fileName);
					// if user chooses no, return
					if(!yesOrNo("%s doesn't exist, would you like to create it with the given text?", fileName)) return;

					// ignore fallthrough to next case
					#if defined(__GNUC__) || defined(__GNUG__)
					__attribute__((fallthrough));
					#elif defined(__clang__)
					[[fallthrough]];
					#endif
				}
				// file exists and is a regular file
				case TYPE_REG_FILE:
				{
					// open file to append
					FILE* file = fopen(fileName, "a+");
		
					// append text if file was opend
					if (file != NULL)
					{
						fprintf(file, "%s", textToAppend);
						fclose(file);
					}
					// file was not opened print error
					else
					{
						printError("\nFailed to open %s.\n\n", fileName);
					}
		
					return;
				}
			}

			return;
		}

		// insert text
		// braces seperate the variable names
		case 'i':
		{
			// should only have 3 args
			if (size != 4)
			{
				printError("\nError: text -i should have 4 arguments including '-i'. Use 'help text' for usage.\n\n");
				return;
			}

			// -i is the last argument, which makes arguments ambiguous
			// print error and return
			if (operationPosition + 1 >= size)
			{
				printError("\nError: text to insert not specified.  Text should come after '-i'. Use 'help text' for usage.\n\n");
				return;
			}

			// text will come after -i switch
			char* textToInsert = args[operationPosition + 1];
			char* fileName;
			int* position = NULL;
			int temp;

			// parse args for int (position)
			// and fileName
			for (int i = 0; i < size; i++)
			{
				if (sscanf(args[i], "%d", &temp) == 1)
				{
					if (position == NULL) position = &temp;
					else
					{
						printError("\nError: Found two positions. Use 'help text' for usage.\n\n");
						return;
					}
				} else
				{
					if (args[i] != textToInsert && i != operationPosition) fileName = args[i];
				}
			}

			// failed to find a position
			if (position == NULL)
			{
				printError("\nError: Could not find a position. Use 'help text' for usage.\n\n");
				return;
			}

			// failed to find a filename
			if (fileName == NULL)
			{
				printError("\nError: Could not find a file name. Use see 'help text' for usage.\n\n");
				return;
			}

			switch (fileInfo(fileName))
			{

				// check file name is a file and not a dir
				case TYPE_REG_DIR:
				{
					printError("\nError: File %s is actually a directory.\n\n", fileName);
					return;
				}

				case TYPE_OTHER:
				{
					printError("\nError: File %s is an unrecognized type.\n\n", fileName);
					return;
				}
	
				// file does not exist ask user if they want to create it
				case FILE_DNE:
				{	
					// if user chooses no, return
					if(!yesOrNo("%s doesn't exist, would you like to create it with the given text and position?", fileName)) return;

					FILE* file = fopen(fileName, "w");

					for (int i = 0; i < *position; i++)
					{
						fputc(' ', file);
					}

					fputs(textToInsert, file);

					fclose(file);
					
					return;
				}

				case TYPE_REG_FILE:
				{
					// open file to append
					FILE* file = fopen(fileName, "r+");
		
					// open temp file
					FILE* tmpFile = tmpfile();
		
					if (tmpFile == NULL)
					{
						printError("\nFailed to create a temp file.\n\n");
						return;
					}
	
					if (file == NULL)
					{
						printError("\nFailed to open %s.\n\n", fileName);
						return;
					}
				
					char c;
	
					// write the first part of the file given
					// to the temp file
					for (int i = 0; i < *position; i++)
					{
						c = getc(file);
	
						// if end of file is reached replace with spaces
						if (c == EOF) c = ' ';
	
						fputc(c, tmpFile);
					}
	
					// insert the new text
					fputs(textToInsert, tmpFile);
	
					// write the rest of the file to tmpFile
					while ((c = getc(file)) != EOF)
					{
						fputc(c, tmpFile);
					}
	
					// reset file positions
					fseek(file, 0, SEEK_SET);
					fseek(tmpFile, 0, SEEK_SET);
					
					// write tmpFile to given file
					while ((c = getc(tmpFile)) != EOF)
					{
						fputc(c, file);
					}
					
					// close the files
					fclose(file);
					fclose(tmpFile);
	
					printMessage("Successfully written.\n");
		
					return;
				}
			}

			return;
		}
		
		// remove all text
		case 'r':
		{

			// should have atleast 2 args including '-r'
			if (size < 2)
			{
				printError("\nError: Text -r should have atleast 2 arguments including '-r'.  Use 'help text' for usage.\n\n");
				return;
			}

			// attempt to use all args as files to clear
			for (int i = 0; i < size; i++)
			{
				// skip the operation
				if (i != operationPosition)
				{
					switch (fileInfo(args[i]))
					{

						// check file name is a file and not a dir
						case TYPE_REG_DIR:
						{
							printError("Error: File %s is actually a directory, skipping.\n", args[i]);
							break;
						}

						case TYPE_OTHER:
						{
							printError("Error: File %s is an unrecognized type, skipping.\n", args[i]);
							break;
						}

						case TYPE_REG_FILE:
						{
							// clear and print success
							fclose(fopen(args[i], "w"));
							printMessage("Cleared %s.\n", args[i]);
							break;
						}
						case FILE_DNE:
						{
							// file did not exist, so print erro
							printMessage("%s does not exist, skipping.\n", args[i]);
							break;
						}
					}
				}
			}
			
			return;
		}

		// display text
		case 'd':
		{
			if (size != 3)
			{
				printError("\nError: Text -d should have 3 arguments including '-d'. Use 'help text' for usage.\n\n");
				return;
			}
		
			char* fileName = NULL;
			int* pageSize = NULL;
			int temp;

			// get size and file name
			for (int i = 0; i < size; i++)
			{
				if (sscanf(args[i], "%d", &temp) == 1)
				{
					if (pageSize == NULL) pageSize = &temp;
					else
					{
						printError("\nError: Found two sizes. Use 'help text' for usage.\n\n");
						return;
					}
				} else
				{
					if (i != operationPosition) fileName = args[i];
				}
			}

			if (pageSize == NULL)
			{
				printError("\nError: Failed to find page size.\n\n");
				return;
			}

			if (fileName == NULL)
			{
				printError("\nError: Failed to find file name.\n\n");
				return;
			}

			switch (fileInfo(fileName))
			{


				case FILE_DNE:
				{
					printError("\nError: File %s does not exist.\n\n", fileName);
					return;
				}
	
				// check file name is a file and not a dir
				case TYPE_REG_DIR:
				{
					printError("\nError: File %s is actually a directory.\n\n", fileName);
					return;
				} 
	
				case TYPE_OTHER:
				{
					printError("\nError: File %s is an unrecognized type.\n\n", fileName);
					return;
				}
				
				case TYPE_REG_FILE:
				{
					int pageIdealSize = getTermHeight() - 10;
		
					// check page size is good
					if (*pageSize < 0)
					{
						printError("\nPage size should be atleast 0.\n\n");
						return;
					} else if (*pageSize > pageIdealSize)
					{
						printError("\nPage size should not be greater than %d.\n\n", pageIdealSize);
						return;
					} else if (*pageSize == 0)
					{
						*pageSize = pageIdealSize;
					}
		
					// open file
					FILE* file = fopen(fileName, "r");
		
					if (file == NULL)
					{
						printError("\nError: Failed to open file %s.\n\n", fileName);
						return;
					}

					int pageWidth = getTermWidth();
		
					// allocate space to read user input
					char* response = malloc(sizeof(char) * 3);

					// allocate space to store each word before printing
					// plus 1 needed to for final \0
					char* word = malloc(sizeof(char) * (pageWidth + 1));
		
					// store the size of each page (up to 1000 pages)
					int* charsPrinted = malloc(sizeof(int) * 1000);
		
					// the page the user is currently on
					int currentPage = 0;

					// boolean to know if on last page
					int onLastPage = 0;
		
					while (1)
					{
						printMessage("\nPage %d:\n\n", currentPage + 1);
					
						int charsPrintedCurrent = 0;
		
						// print term height - 10 lines
						for (int i = 0; i < *pageSize; i++)
						{
						
							// print the row
							for (int j = 0; j < pageWidth; j++)
							{
								char c = getc(file);
								if (c != EOF) charsPrintedCurrent++;
			
								// handle different cases of characters
								switch (c)
								{
									// new line, increment the row position (i)
									// and reset the column position (j)
									case '\n':
									{
										j = 0;
										i++;

										putc(c, stdout);
			
										// account for windows stupid line endings
										// that are \n\r instead of \n
										#if defined(_WIN32) || defined(_WIN64)
										charsPrintedCurrent++;
										#endif
				
										// break out of loop if lines
										// limit is reached
										if (i >= *pageSize) j = pageWidth;
										break;
									}

									case EOF:
									{
										// instead of EOF print space
										putc(' ', stdout);
										// set onlast page to stop going forward
										// when on last page
										onLastPage = 1;
										break;
									}

									default:
									{
										// print char
										// replace \t with 4 spaces
										if (c != '\t') putc(c, stdout);
										else
										{
											printf("    ");
											j += 3;
										}
										break;
									}
								}
							}
						}
		
						// record the number of chars on this page
						charsPrinted[currentPage] = charsPrintedCurrent;
		
						// print an extra newline
						printf("\n");
		
						// handle user input
					
						*response = '\0';
		
						while (*response == '\0')
						{
							// print the question
							printMessage("\nNext page, previouse page, reprint current page, or stop (N/p/r/s): ");
		
							// get the response
							fgets(response, 3, stdin);
					
							// change it to lower case
							*response = tolower(*response);
					
							// check that use has entered 0 or 1 chars
							if (*response != '\n' && *(response + 1) != '\n')
							{
								// print error and reset response
								printError("\nPlease only enter 0 or 1 chars.\n");
								*response = '\0';
					
								// clear stdin
								char c;
								while ((c = getchar()) != '\n' && c != EOF);
							}
					
							// only accept when response is valid
							switch (*response)
							{
								// next
								case 'n':
								// choose default
								case '\n':
								{
									if (!onLastPage)
									{
										currentPage++;
									} else
									{
										printError("\nError: You're on the last page.\n");
										*response = '\0';
									}
									break;
								}
								// previous
								case 'p':
								{
									if (currentPage > 0)
									{
										// reset being on last page
										onLastPage = 0;
										// decrement current page
										currentPage--;
										// move file back by as many chars were printed + 2 as seek moves will skip the first two chars
										fseek(file, -1 * (charsPrintedCurrent + charsPrinted[currentPage]), SEEK_CUR);
									} else
									{
										printError("\nYou're at the first page.\n");
										*response = '\0';
									}
									break;
								}
								// reprint
								case 'r':
								{
									fseek(file, -1 * charsPrintedCurrent, SEEK_CUR);
									break;
								}
								// stop
								case 's':
									// print newline and free stuff
									printf("\n");
									free(charsPrinted);
									free(response);
									free(word);
									return;
			
								// error not valid
								default:
								{
									if (*response != '\0')
									{
										printError("\n%c is not a valid choice.\n", *response);
										*response = '\0';
									}
									break;
								}
							}
						}
					}
					
					printf("\n");

					// free allocated stuff
					free(charsPrinted);
					free(response);
				}
			}

			return;
		}

		// unrecognized option
		default:
		{
			printError("\nError: Unrecognized option, use 'help text' for usage.\n\n");
			return;
		}
	}
}

void cd(char** args, int size)
{
	switch(size)
	{
		// no directory specified
		case 0:
		{
			printError("\nError: Please specify a directory.\n\n");
			return;
		}

		case 1:
		{
			// get file info using first arg
			switch (fileInfo(args[0]))
			{
				// check directory/file exists
				case FILE_DNE:
				{
					printError("\nDirectory %s does not exist.\n\n", args[0]);
					return;
				}
	
				// arg is not a regular dir
				case TYPE_REG_FILE:
				case TYPE_OTHER:
				{
					printError("\n'%s' is not a directory.\n\n", args[0]);
					return;
				}
	
				// attempt to change to directory
				// print error on failure
				case TYPE_REG_DIR:
				{
					if (chdir(args[0])) printError("\nError: Failed to change directory.\n\n");
				}
			}

			return;
		}

		default:
		{
			// too many args given
			printError("\nError: Too many arguments.\n\n");
			break;
		}
	}
}
