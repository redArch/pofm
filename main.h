#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>
#endif

// include stdarg for va_list type
#include <stdarg.h>

#define PATH_MAX 4096
#define COMMAND_MAX 1024
#define FUNCTION_MAX 32
#define ARGS_MAX 16

#define TYPE_REG_FILE 0
#define TYPE_REG_DIR  1
#define TYPE_OTHER    3
#define FILE_DNE      4

#define VERSION "1.0.0"

typedef struct
{
	char* name;
	char* helpDescription;
	char* helpOptions;
	void (*function)(char** args, int size);
} Command;

extern Command commands[];
extern int commandsSize;

#if defined(_WIN32) || (_WIN64)
extern HANDLE con;
#endif

// functions that commands call, the args will be an
// array of strings containing the options
// the size is ther size of the array
void help(char** args, int size);
void quit(char** args, int size);
void newFile(char** args, int size);
void deleteFile(char** args, int size);
void renameFile(char** args, int size);
void copyFile(char** args, int size);
void moveFile(char** args, int size);
void textFile(char** args, int size);
void version(char** args, int size);

// additional function(s) not in the doc
void cd(char** args, int size);
void ls(char** args, int size);

// check the filetype (file or directory)
int fileInfo(char* fileName);

// print a message
void printMessage(char* messageFormat, ...);
void printMessageInternal(char* messageFmt, va_list argslist);

// print error
void printError(char* errorFmt, ...);
