main: main.c
	gcc -Wall -Wextra -std=c99 -o pofm *.c

debug: main.c
	gcc -g -Wall -Wextra -std=c99 -o pofm *.c