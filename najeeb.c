#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "main.h"
#if defined(_WIN32) || defined(_WIN64)
#include <io.h>
#else
#include <sys/types.h>
#include <sys/stat.h>
#endif

// handle directories (-d)
//This Function created a file
void newFile(char** args, int size)
{
	char operation = '\0';
	int operationPosition;

	for (int i = 0; i < size; i++)
	{
		//find the option(s)
		if (*(args[i]) == '-')
		{
			//if user tries to use multiple options print error
			if (strlen (args [i]) != 2 || operation != '\0')
			{
				printError("\nError\n");
				return;
			}

			//set operation
			operation = *(args[i] + 1);
			operationPosition = i;
		}
	}

	//no operation found
	if (operation != '\0' && operation != 'd')
	{
		printError("\nError new only accepts -d as an option\n");
		return;
	}
	
	for (int i = 0; i < size; i++)
	{
		if (i != operationPosition)
		{
		//Sends argument entered to fileInfo function
		//to check if argument is of what type file or directory
		// and whether it exists or not
			switch (fileInfo(args[i]))
			{	
				//Case when File does not exist
				case FILE_DNE: 
				{
					//Based on user input: Creates Directory
					if (operation == 'd')
					{
						int d;
						//Createss a new directory to support 
						//Windows 32 or 64
						//Unix
						#if defined(_WIN32) || defined(_WIN64)
						d = mkdir(args[i]);
						#else
						d = mkdir(args[i], 0755);
						#endif

						//Directory successfully created
						if (!d)
						{
							printMessage("Directory created\n");
							return;
						}
						//Directory creation unsuccessful
						else
						{
							printMessage("Unable to create\n");
							return;
						}
					}
					//else  Creates File
					else
					{	//Create a file take arg[i] to 'w' Write.
						FILE *fp = fopen(args[i], "w");

						//If file not created									
						if (fp == NULL)
						{
							printError("\nUnsuccessful. File not created.\n\n");
							return;
						}
						//Else file created
						else
						{
							printMessage("File %s created.\n", args[i]);
						}

						//Close file
						fclose(fp);
					}

					break;				
				}	

				//Case when file already exists
				case TYPE_REG_FILE:
				{
					printMessage("\nFile already exists.\n");
					break;
					
				}
				
				//Case when argument entered is a directory
				case TYPE_REG_DIR:
				{
					printMessage("\nFile is a directory.\n");
					break;
				}

				//Case when argument entered is another type
				case TYPE_OTHER:
				{
					printMessage("\nFile is another type.\n");
					break;
				}
			}
		}
	}
}

// TODO
// handle directories (must use -r)
//This function copies a file to another file
void copyFile(char** args, int size)
{

	if(size != 2)
	{
		printMessage("\nPlease enter 2 arguments or use 'help copy'.\n\n");
	}

	else
	{
		char copy;

		FILE *source = fopen(args[0], "r");
	
		if(source == NULL)
		{
			printMessage("\nUnsuccessful.\n\n");
			return;
		}

		FILE *destination = fopen(args[1], "w");

		if(source == NULL)
		{
			printError("\nUnsuccessful.\n\n");
			return;
		}
	
		while ((copy = fgetc(source)) != EOF)
		{
			fputc(copy, destination);
		}

		printMessage("Successfully copied.\n");
	
		fclose(source);
		fclose(destination);

		return;
	}
	
}
