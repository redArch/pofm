#include <stdio.h>
#include <dirent.h>
#include "main.h"

// rename [oldFile] [newFile]
void renameFile(char** args, int size)
{
	switch(size)
	{
		case 2:
		{
			if(fileInfo(args[0]) == FILE_DNE)
			{
				printError("\nError: File '%s' does not exist.\n\n", args[0]);
				return;
			}

			// rename old file with new name
			if (rename(args[0], args[1]) == 0)
			{
				printMessage("File renamed successfully.\n");
				return;
			}
			else
			{
				printError("Failed to rename file.\n");
				return;
			}
		}

		default:
		{
			printError("Error: Expected 2 arguments. Please use 'help rename' for usage.\n");
			return;
		}
	}
}

// ls
// ls [directory]
void ls(char** args, int size)
{
	DIR *d;
	struct dirent *dir;

	switch (size)
	{
		case 0: //list the files in the current directory
		{
			d = opendir(".");
			if (d)
			{
				while ((dir = readdir(d)) != NULL)
				{
					if (fileInfo(dir->d_name) != TYPE_REG_DIR) printf("%s\n", dir->d_name);
					else printMessage("%s\n", dir->d_name);
				}
			closedir(d);
			}
			return;
		}
		case 1: //list the files in the specified path
		{
			d = opendir(args[0]);
	
			// Unable to open directory stream
			if (!d)
			{
				printError("Error: Directory: '%s' not found.\n", args[0]);
				return;
			}
	
			while ((dir = readdir(d)) != NULL)
			{
					if (fileInfo(dir->d_name) != TYPE_REG_DIR) printf("%s\n", dir->d_name);
					else printMessage("%s\n", dir->d_name);
			}
	
			// Close directory stream
			closedir(d);
			return;
		}
		default:
		{
			printError("Error: Expected 0 or 1 argument. Please use 'help ls' for usage.\n");
			return;
		}
	}
}
