#if defined(_WIN32) || defined(_WIN64)
	#include <windows.h>
#else
	#include <sys/ioctl.h>
#endif

#include <unistd.h>

int getTermWidth()
{
	#if defined(_WIN32) || defined(_WIN64)
		CONSOLE_SCREEN_BUFFER_INFO csbi;
		GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);

		return csbi.srWindow.Right - csbi.srWindow.Left + 1;
	#else
		struct winsize w;
		ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

		return w.ws_col;
	#endif
}

int getTermHeight()
{
	#if defined(_WIN32) || defined(_WIN64)
		CONSOLE_SCREEN_BUFFER_INFO csbi;
		GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);

		return csbi.srWindow.Bottom - csbi.srWindow.Top + 1;
	#else
		struct winsize w;
		ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

		return w.ws_row;
	#endif
}